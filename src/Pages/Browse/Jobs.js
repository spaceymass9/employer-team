import React from "react";
import JobsButton from "./JobsButton";

function Jobs(props) {
  console.log(props);

  return (
    <div>
      <div class="card mt-3 border-0">
        <h4>ACTIVE JOBS</h4>
        <button variant="btn-info" size="lg" active>
          ADD NEW JOB
        </button>
      </div>
      <div class="card text-center mt-3">
        <div class="card-body">
          <h5>{props.cardTitle}</h5>
          <h5>{props.cardTitle}</h5>
          <p>{props.cardText}</p>
        </div>
      </div>
    </div>
  );
}

export default Jobs;
