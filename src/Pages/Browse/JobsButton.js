import React from "react";
import "./Jobs";

function JobsButton(props) {
  return (
    <div class="m-auto">
      <button label="Active Jobs" role="ADD NEW JOB" />
    </div>
  );
}

export default JobsButton;
