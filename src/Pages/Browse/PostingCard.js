import React from "react";
import "./Jobs";

function PostingCard() {
  return (
    <div className="card text-center mt-3">
      <PostingCard
        cardTitle='<a href="/goodwill-classroom/project-demopg3.html">Front End Developer</a>'
        cardTitle="Confidential - Charlotte, NC"
        cardText="Strong working experience with HTML5 and web template engines – Markdown, Handlebars, etc.
                Expert level experience with browser-based technology, user interface…"
      />
      <PostingCard
        cardTitle='<a href="/goodwill-classroom/project-demopg3.html">Junior Web Designer and Front-End Developer</a>'
        cardTitle="Fame Foundry - Charlotte, NC 28210 (Quail Hollow area)"
        cardText="Fame Foundry is seeking a web designer/front-end developer with a fundamental understanding of full-cycle web development to join our team."
      />

      <PostingCard
        cardTitle='<a href="/goodwill-classroom/project-demopg3.html">Junior BI Developer'
        cardTitle="Discovery Education - Charlotte, NC"
        cardText="A minimum of 6 months experience developing full stack in a web application framework:
        Discovery Education is the global leader in standards-based digital…"
      />
    </div>
  );
}

/*
      <PostingCard
        cardTitle='<a href="/goodwill-classroom/project-demopg3.html">Front End Developer</a>'
        cardTitle="Confidential - Charlotte, NC"
        cardText="Strong working experience with HTML5 and web template engines – Markdown, Handlebars, etc.
                Expert level experience with browser-based technology, user interface…"
      />
      <PostingCard
        cardTitle='<a href="/goodwill-classroom/project-demopg3.html">Junior Web Designer and Front-End Developer</a>'
        cardTitle="Fame Foundry - Charlotte, NC 28210 (Quail Hollow area)"
        cardText="Fame Foundry is seeking a web designer/front-end developer with a fundamental understanding of full-cycle web development to join our team."
      />

      <PostingCard
        cardTitle='<a href="/goodwill-classroom/project-demopg3.html">Junior BI Developer'
        cardTitle="Discovery Education - Charlotte, NC"
        cardText="A minimum of 6 months experience developing full stack in a web application framework:
        Discovery Education is the global leader in standards-based digital…"
      />
      */

export default PostingCard;
