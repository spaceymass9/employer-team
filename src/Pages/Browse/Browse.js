import React from "react";
import { Link } from "react-router-dom";

function Browse() {
  return (
    <div
      className="jumbotron jumbotron-fluid bg-white d-flex justify-content-center align-items-center"
      style={{ height: 500 + "px" }}
    >
      <div className="container">
        <label className="pt-2">
          <h4 className="pr-3">ACTIVE JOBS</h4>
        </label>
        <Link className="btn btn-info pt-2 btn-lg" to="/FormPage" role="button">
          ADD NEW JOB
        </Link>

        <div className="card text-center mt-3">
          <h5>
            <a href="/goodwill-classroom/project-demopg3.html">
              Front End Developer
            </a>
          </h5>
          <h5 className="card-title">Confidential - Charlotte, NC</h5>
          <p className="card-text">
            Strong working experience with HTML5 and web template engines –
            Markdown, Handlebars, etc. Expert level experience with
            browser-based technology, user interface…
          </p>
        </div>

        <div className="card text-center mt-3">
          <div className="card-body">
            <h5>
              <a href="#">Junior Web Designer & Front-End Developer</a>
            </h5>
            <h5>Fame Foundry - Charlotte, NC 28210 (Quail Hollow area)</h5>
            <p className="card-text">
              Fame Foundry is seeking a web designer/front-end developer with a
              fundamental understanding of full-cycle web development to join
              our team.
            </p>
          </div>
        </div>

        <div className="card text-center mt-3">
          <div className="card-body">
            <h5>
              <a href="#">Junior BI Developer</a>
            </h5>
            <h5>Discovery Education - Charlotte, NC</h5>
            <p className="card-text">
              A minimum of 6 months experience developing full stack in a web
              application framework: Discovery Education is the global leader in
              standards-based digital…
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Browse;
