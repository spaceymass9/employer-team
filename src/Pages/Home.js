import React from "react";

function Home() {
  return (
    <div className="jumbotron jumbotron-fluid bg-white d-flex justify-content-center align-items-center">
      <div className="container">
        <h1 className="display-4 text-center">
          MISSION<span className="bg-info">.code</span>
        </h1>

        <div className="form-group pt-3">
          <input
            type="text"
            className="form-control"
            id="formGroupExampleInput"
            placeholder="Search for job postings by keyword, job titile, or company"
          />
          <i className="fas fa-search"></i>
        </div>
      </div>
    </div>
  );
}

export default Home;
