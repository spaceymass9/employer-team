import React from "react";
import { Link } from "react-router-dom";

function Form() {

  React.useEffect(() => {
    const jobsRef = window.firebase.database().ref('/jobs');

    jobsRef.on('value', function (snapshot) {
      const jobsFromDb = snapshot.val();
      setJobs(jobsFromDb);
      // console.log('jobs from db', jobsFromDb);
    });

  }, []);

  const [jobs, setJobs] = React.useState([]);
  const [action, setAction] = React.useState('init');
  return (
    <div className="container">
      <label className="pt-2">
        <h4 className="pr-3">ADD NEW JOB</h4>
      </label>
      <Link className="btn btn-info pt-2 btn-lg" to="/Browse" role="button">
        ACTIVE JOBS
      </Link>

      <div className="col-sm-6 my-2">
        <div className={action === 'init' ? 'alert alert-success invisible' : 'alert alert-success visible'} role="alert">
          New Job has been posted successfully
        </div>
      </div>

      <form onSubmit={handleFormSubmit} id="jobpost">
        <h3 className="text-left">New Job Posting</h3>
        <div className="mt-4">
          <div className="form-group row ">
            <label htmlFor="jobtitle" className="col-sm-2 col-form-label">
              Job Title
            </label>
            <div className="col-sm-4">
              <input
                required
                type="name"
                className="form-control"
                id="jobtitle"
                placeholder="Enter job title"
              ></input>
            </div>
          </div>
          <div className="form-group row ">
            <label htmlFor="joblocation" className="col-sm-2 col-form-label">
              Job Location
            </label>
            <div className="col-sm-4">
              <input
                required
                type="name"
                className="form-control"
                id="joblocation"
                placeholder="Charlotte, NC"
              ></input>
            </div>
          </div>

          <div className="form-group row">
            <label htmlFor="minsal" className="col-sm-2 col-form-label">
              Salary Range
            </label>
            <div className="col-sm-2">
              <input
                required
                type="number"
                className="form-control"
                id="minsal"
                placeholder="$0"
              ></input>
            </div>
            <div className="col-sm-2">
              <input
                required
                type="number"
                className="form-control"
                id="maxsal"
                placeholder="$000,000"
              ></input>
            </div>
          </div>

          <div className="form-group row">
            <label htmlFor="jobdesc" className="col-sm-2 col-form-label">
              Job Description
            </label>
            <div className="col-sm-4">
              <textarea
                required
                className="form-control"
                id="jobdesc"
                rows="4"
              ></textarea>
            </div>
          </div>

          <div className="form-group row">
            <label htmlFor="recruiter" className="col-sm-2 col-form-label">
              Recruiter Name
            </label>
            <div className="col-sm-4">
              <input
                type="text"
                className="form-control"
                id="recruiter"
                placeholder=""
              ></input>
            </div>
          </div>
        </div>

        <div className="button-wrapper pt-4">
          <button type="reset" className="btn btn-md greynav">
            Cancel
        </button>
          <button type="submit" className="btn btn-info btn-md ml-3">
            Continue
        </button>
        </div>

      </form>
    </div>
  );

  function handleFormSubmit($event) {
    $event.preventDefault();
    let jobCreatedDate = new Date();
    jobCreatedDate = jobCreatedDate.toDateString().split(' ');
    jobCreatedDate.shift();
    jobCreatedDate = jobCreatedDate.join('-');
    const jobId = jobs.length;
    const jobTitle = document.querySelector('#jobtitle').value;
    const jobLocation = document.querySelector('#joblocation').value;
    const jobDesc = document.querySelector('#jobdesc').value;
    const jobMinSal = document.querySelector('#minsal').value;
    const jobMaxSal = document.querySelector('#maxsal').value;
    const recruiter = document.querySelector('#recruiter').value;

    const newJob = {
      id : jobId,
      date: jobCreatedDate,
      description: jobDesc,
      location: jobLocation,
      minSalary: jobMinSal,
      maxSalary: jobMaxSal,
      recruiter: recruiter,
      title: jobTitle
    };

    setAction('updated');
    document.querySelector('#jobpost').reset();

    setTimeout(() => {
        setAction('init');
    }, 3000);
    
    const updatedRecord = {};
    updatedRecord['/jobs/' + jobId] = newJob;
    window.firebase.database().ref().update(updatedRecord);

  }
}

export default Form;
