import React from "react";
import { Link } from "react-router-dom";

function Nav() {
  return (
    <nav
      className="navbar navbar-expand-lg navbar-dark  greynav"
      style={{ height: 120 + "px" }}
    >
      <div className="navbar-brand text-dark bg-light p-3">
        <span style={{ height: 5 + "px" }}>
          <h1 className="text-center">
            MISSION<span className="bg-info">.code</span>
          </h1>
        </span>
      </div>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarText"
        aria-controls="navbarText"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarText">
        <ul className="navbar-nav ml-auto p-3">
          <li className="nav-item active">
            <Link className="nav-link text-dark" to="/home">
              HOME <span className="sr-only">(current)</span>
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-dark" to="/FormPage">
              ADD NEW JOB
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-dark" to="/Browse">
              BROWSE JOBS
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-dark" to="/FormPage">
              PROFILE
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link text-dark" to="/home">
              LOG OUT
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Nav;
