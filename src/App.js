import React from "react";
import Nav from "./Shared/Navigation/Nav";
import Footer from "./Shared/Footer/Footer";
import Browse from "./Pages/Browse/Browse";
import Form from "./Pages/FormPage/Form";
import Home from "./Pages/Home";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import JobsButton from "./Pages/Browse/JobsButton";
import PostingCard from "./Pages/Browse/PostingCard";

function Main() {
  return (
    <BrowserRouter>
      <main>
        <Nav />
        <Switch>
          <Route path="/home">
            <Home />
          </Route>
          <Route path="/FormPage">
            <Form />
          </Route>
          <Route path="/Browse">
            <Browse />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>

        <Footer />
      </main>
    </BrowserRouter>
  );
}

export default Main;
